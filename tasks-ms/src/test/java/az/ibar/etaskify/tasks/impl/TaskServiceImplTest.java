package az.ibar.etaskify.tasks.impl;

import az.ibar.common.dto.UserResponseDto;
import az.ibar.common.security.auth.service.SecurityService;
import az.ibar.common.security.exception.UserNotFoundException;
import az.ibar.etaskify.tasks.client.UserManagementClient;
import az.ibar.etaskify.tasks.domain.task.Status;
import az.ibar.etaskify.tasks.domain.task.Task;
import az.ibar.etaskify.tasks.domain.task.TaskAssigneeUsers;
import az.ibar.etaskify.tasks.repository.TaskRepository;
import az.ibar.etaskify.tasks.service.dto.TaskRequestDto;
import az.ibar.etaskify.tasks.service.dto.TaskResponseDto;
import az.ibar.etaskify.tasks.service.impl.TaskServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.data.annotation.Id;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TaskServiceImplTest {

    private static final String USERNAME = "johndoe";
    private static final Long ID = 1L;
    private static final String AUTHORISATION_HEADER = "Authorization";

    @Spy
    private ModelMapper modelMapper;

    @Mock
    private TaskRepository taskRepository;

    @Mock
    private UserManagementClient userManagementClient;

    @Mock
    private HttpServletRequest servletRequest;

    @Mock
    private SecurityService securityService;

    @InjectMocks
    private TaskServiceImpl taskService;


    @Test
    void givenInputIsValidWhenCreateThenSave() {
        // Arrange
        final TaskRequestDto taskRequestDto = prepareTaskRequestDto();
        final Task task = modelMapper.map(taskRequestDto, Task.class);

        when(securityService.getCurrentUserLogin()).thenReturn(Optional.of(USERNAME));
        when(servletRequest.getHeader(AUTHORISATION_HEADER)).thenReturn("Token");
        when(userManagementClient
                .checkUsersExist(servletRequest.getHeader(AUTHORISATION_HEADER), taskRequestDto.getAssigneeUsers()))
                .thenReturn(new ResponseEntity<>(true, HttpStatus.OK));
        when(userManagementClient
                .getUsersById(servletRequest.getHeader(AUTHORISATION_HEADER), taskRequestDto.getAssigneeUsers()))
                .thenReturn(new ResponseEntity<>(List.of(UserResponseDto.builder()
                        .id(ID)
                        .name("name")
                        .email("email@gmail.com")
                        .build()), HttpStatus.OK));

        String username = securityService.getCurrentUserLogin().orElse(null);
        task.setCreatedBy(username);
        task.setLastModifiedBy(username);
        List<TaskAssigneeUsers> assigneeUsers = List.of(populateAssigneeUser(task));
        task.setAssigneeUsers(assigneeUsers);

        final Task savedTask = prepareTask();
        when(taskRepository.save(any())).thenReturn(savedTask);

        //Act
        TaskResponseDto result = taskService.create(taskRequestDto);

        //Assert
        verify(taskRepository, times(1)).save(any());
        assertThat(result.getTitle()).isEqualTo(task.getTitle());
        assertThat(result.getDescription()).isEqualTo(task.getDescription());
        assertThat(result.getDeadLine()).isEqualTo(task.getDeadLine());
    }

    @Test
    void whenAssignTaskThenAssignTask() {
        //Arrange
        final Task task = prepareTask();
        final List<Long> assignUserList = List.of(ID);

        when(servletRequest.getHeader(AUTHORISATION_HEADER)).thenReturn("Token");
        when(userManagementClient
                .checkUsersExist(servletRequest.getHeader(AUTHORISATION_HEADER), assignUserList))
                .thenReturn(new ResponseEntity<>(true, HttpStatus.OK));
        when(taskRepository.findById(ID)).thenReturn(Optional.of(task));
        when(userManagementClient
                .getUsersById(servletRequest.getHeader(AUTHORISATION_HEADER), assignUserList))
                .thenReturn(new ResponseEntity<>(List.of(UserResponseDto.builder()
                        .id(ID)
                        .name("name")
                        .email("email@gmail.com")
                        .build()), HttpStatus.OK));

        final Task savedTask = prepareTask();
        TaskAssigneeUsers assigneeUser = populateAssigneeUser(savedTask);
        task.setAssigneeUsers(List.of());
        when(taskRepository.save(any())).thenReturn(savedTask);

        //Act
        TaskResponseDto result = taskService.assignTask(ID, assignUserList);

        //Assert
        verify(taskRepository, times(1)).save(any());
        assertThat(result.getTitle()).isEqualTo(task.getTitle());
        assertThat(result.getDescription()).isEqualTo(task.getDescription());
        assertThat(result.getDeadLine()).isEqualTo(task.getDeadLine());
        assertThat(result.getStatus()).isEqualTo(task.getStatus().name());
        assertThat(result.getCreatedBy()).isEqualTo(task.getCreatedBy());
        assertThat(result.getLastModifiedBy()).isEqualTo(task.getLastModifiedBy());
        assertThat(result.getAssigneeUsers().get(0).getId()).isEqualTo(assigneeUser.getUserId());
    }

    @Test
    void whenAssignTaskOtherOrganisationUserThenDontAssign() {
        //Arrange
        final Task task = prepareTask();
        final List<Long> assignUserList = List.of(ID);

        when(servletRequest.getHeader(AUTHORISATION_HEADER)).thenReturn("Token");
        when(userManagementClient
                .checkUsersExist(servletRequest.getHeader(AUTHORISATION_HEADER), assignUserList))
                .thenReturn(new ResponseEntity<>(false, HttpStatus.OK));
        when(taskRepository.findById(ID)).thenReturn(Optional.of(task));

        // Act && Assert
        assertThatThrownBy(() -> taskService.assignTask(ID, assignUserList))
                .isInstanceOf(UserNotFoundException.class);
    }

    private TaskRequestDto prepareTaskRequestDto() {
        return TaskRequestDto.builder()
                .title("title")
                .description("description")
                .status(Status.OPEN)
                .deadLine(LocalDate.EPOCH)
                .assigneeUsers(List.of(ID))
                .build();
    }

    private Task prepareTask() {
        return Task.builder()
                .id(ID)
                .title("title")
                .description("description")
                .status(Status.OPEN)
                .deadLine(LocalDate.EPOCH)
                .assigneeUsers(List.of(TaskAssigneeUsers.builder()
                        .userId(ID)
                        .build()))
                .build();
    }

    private TaskAssigneeUsers populateAssigneeUser(Task task) {
        return TaskAssigneeUsers.builder()
                .task(task)
                .userId(ID)
                .build();
    }
}
