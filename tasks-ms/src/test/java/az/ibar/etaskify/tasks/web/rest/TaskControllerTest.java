package az.ibar.etaskify.tasks.web.rest;

import az.ibar.common.dto.UserResponseDto;
import az.ibar.etaskify.tasks.domain.task.Status;
import az.ibar.etaskify.tasks.service.TaskService;
import az.ibar.etaskify.tasks.service.dto.TaskRequestDto;
import az.ibar.etaskify.tasks.service.dto.TaskResponseDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(TaskController.class)
public class TaskControllerTest {

    private static final Long ID = 1L;
    private static final String BASE_URL = "/tasks";
    private static final String CONTENT_TYPE_JSON = "application/json";
    private static final String ROLE_ADMIN = "ADMIN";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private TaskService taskService;

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void givenValidInputWhenCreateThenCreated() throws Exception {
        // Arrange
        final TaskResponseDto taskResponseDto = prepareTaskResponseDto();
        final TaskRequestDto taskRequestDto = prepareTaskRequestDto();
        when(taskService.create(taskRequestDto)).thenReturn(taskResponseDto);

        // Action
        ResultActions actions = mockMvc.perform(
                post(BASE_URL)
                        .contentType(CONTENT_TYPE_JSON)
                        .content(objectMapper.writeValueAsString(taskRequestDto)))
                .andExpect(status().isCreated());

        // Assert
        verify(taskService, times(1)).create(taskRequestDto);
        actions.andExpect(jsonPath("title", is(taskRequestDto.getTitle()), String.class));
        actions.andExpect(jsonPath("description", is(taskRequestDto.getDescription()), String.class));
        actions.andExpect(jsonPath("deadLine", is(taskRequestDto.getDeadLine().toString()), String.class));
        actions.andExpect(jsonPath("status", is(taskRequestDto.getStatus().name()), String.class));
    }


    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void whenAssignTaskThenAssignUserToTask() throws Exception {
        // Arrange
        final TaskResponseDto taskResponseDto = prepareTaskResponseDto();
        final TaskRequestDto taskRequestDto = prepareTaskRequestDto();
        when(taskService.assignTask(ID, List.of(ID)))
                .thenReturn(taskResponseDto);

        // Action
        ResultActions actions = mockMvc
                .perform(put(BASE_URL + "/" + ID)
                        .param("userId", "1")).andExpect(status().isOk());

        // Assert
        verify(taskService,
                times(1)).assignTask(ID, List.of(ID));

        actions.andExpect(status().isOk());
//        actions.andExpect(jsonPath("assigneeUsers", is(), String.class));
//        actions.andExpect(jsonPath("assigneeUsers",is(objectMapper.writeValueAsString(UserResponseDto.builder()
//                .id(ID)
//                .name("name")
//                .email("email@gmail.com")
//                .build())), String.class));

    }

    private TaskRequestDto prepareTaskRequestDto() {
        return TaskRequestDto.builder()
                .title("title")
                .description("description")
                .status(Status.OPEN)
                .deadLine(LocalDate.now())
                .assigneeUsers(List.of(ID))
                .build();
    }

    private TaskResponseDto prepareTaskResponseDto() {
        return TaskResponseDto.builder()
                .id(ID)
                .createdBy("admin")
                .createdDate(Instant.now())
                .deadLine(LocalDate.now())
                .description("description")
                .status("OPEN")
                .title("title")
                .assigneeUsers(List.of(UserResponseDto.builder()
                        .id(ID)
                        .name("name")
                        .email("email@gmail.com")
                        .build()))
                .build();
    }
}
