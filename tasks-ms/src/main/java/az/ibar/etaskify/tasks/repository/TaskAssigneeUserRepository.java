package az.ibar.etaskify.tasks.repository;

import az.ibar.etaskify.tasks.domain.task.TaskAssigneeUsers;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskAssigneeUserRepository extends JpaRepository<TaskAssigneeUsers, Long> {
}
