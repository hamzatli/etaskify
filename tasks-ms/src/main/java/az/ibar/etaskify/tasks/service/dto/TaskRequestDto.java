package az.ibar.etaskify.tasks.service.dto;

import az.ibar.etaskify.tasks.domain.task.Status;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TaskRequestDto {

    @NotBlank
    private String title;
    private String description;
    private LocalDate deadLine;
    private Status status;
    private List<Long> assigneeUsers;
}
