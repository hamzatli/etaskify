package az.ibar.etaskify.tasks.config;

import az.ibar.common.config.ModelMapperConfig;
import az.ibar.common.exception.GlobalExceptionHandler;
import az.ibar.common.security.auth.service.SecurityService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Import({ModelMapperConfig.class, GlobalExceptionHandler.class})
@Configuration
public class Config {

    @Bean
    public SecurityService getSecurityService(){
        return new SecurityService();
    }
}
