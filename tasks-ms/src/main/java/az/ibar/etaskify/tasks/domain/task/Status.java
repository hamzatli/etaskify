package az.ibar.etaskify.tasks.domain.task;

public enum Status {
    OPEN, PROCESSING, TESTING, PROBLEM, CLOSED
}
