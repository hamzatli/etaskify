package az.ibar.etaskify.tasks.config;

import az.ibar.common.security.UserAuthority;
import az.ibar.common.security.auth.AuthenticationEntryPointConfigurer;
import az.ibar.common.security.auth.service.AuthService;
import az.ibar.common.security.auth.service.JwtService;
import az.ibar.common.security.config.BaseSecurityConfig;
import az.ibar.common.security.config.SecurityProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

import java.util.List;

@Slf4j
@Import({SecurityProperties.class, JwtService.class,
        AuthenticationEntryPointConfigurer.class})
@EnableWebSecurity
public class SecurityConfiguration extends BaseSecurityConfig {

    private static final String TASK_API = "/v1/tasks/**";

    public SecurityConfiguration(SecurityProperties securityProperties, List<AuthService> authServices) {
        super(securityProperties, authServices);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(HttpMethod.POST, TASK_API).access(authorities(UserAuthority.ADMIN))
                .antMatchers(HttpMethod.PUT, TASK_API).access(authorities(UserAuthority.ADMIN))
                .antMatchers(HttpMethod.DELETE, TASK_API).access(authorities(UserAuthority.ADMIN))
                .antMatchers(HttpMethod.GET).permitAll();

        super.configure(http);
    }
}
