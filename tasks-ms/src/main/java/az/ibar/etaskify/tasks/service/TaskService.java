package az.ibar.etaskify.tasks.service;

import az.ibar.common.dto.GenericSearchDto;
import az.ibar.etaskify.tasks.service.dto.TaskRequestDto;
import az.ibar.etaskify.tasks.service.dto.TaskResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface TaskService {

    TaskResponseDto create(TaskRequestDto taskRequestDto);

    TaskResponseDto assignTask(Long taskId, List<Long> assigneeUserIds);

    Page<TaskResponseDto> search(GenericSearchDto genericSearchDto, Pageable pageable);
}
