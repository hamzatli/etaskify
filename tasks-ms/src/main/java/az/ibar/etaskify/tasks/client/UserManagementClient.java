package az.ibar.etaskify.tasks.client;

import az.ibar.common.dto.UserResponseDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Set;

@FeignClient(name = "user-management-ms", url = "http://localhost:8080/users")
public interface UserManagementClient {

    @GetMapping("/v1/users/check")
    ResponseEntity<Boolean> checkUsersExist(@RequestHeader("Authorization") String authorization,
                                            @RequestParam List<Long> ids);

    @GetMapping("/v1/users/bulk")
    ResponseEntity<List<UserResponseDto>> getUsersById(@RequestHeader("Authorization") String authorization,
                                                       @RequestParam List<Long> ids);
}