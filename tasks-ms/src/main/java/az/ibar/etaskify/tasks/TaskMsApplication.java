package az.ibar.etaskify.tasks;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@Slf4j
@SpringBootApplication
@EnableFeignClients("az.ibar.etaskify.tasks.client")
public class TaskMsApplication {

    public static void main(String[] args) {
        SpringApplication.run(TaskMsApplication.class, args);
    }
}
