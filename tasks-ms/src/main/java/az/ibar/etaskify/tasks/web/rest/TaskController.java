package az.ibar.etaskify.tasks.web.rest;

import az.ibar.common.dto.GenericSearchDto;
import az.ibar.etaskify.tasks.service.TaskService;
import az.ibar.etaskify.tasks.service.dto.TaskRequestDto;
import az.ibar.etaskify.tasks.service.dto.TaskResponseDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@RequestMapping("/tasks")
@RestController
public class TaskController {

    private final TaskService taskService;

    @PostMapping
    public ResponseEntity<TaskResponseDto> createTask(@RequestBody @Valid TaskRequestDto taskRequestDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(taskService.create(taskRequestDto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<TaskResponseDto> assignTask(@PathVariable Long id,
                                                      @RequestParam List<Long> userId) {
        return ResponseEntity.ok(taskService.assignTask(id, userId));
        //TODO add custom exception if admin cannot assign user
    }

    @PostMapping("/search")
    public ResponseEntity<Page<TaskResponseDto>> search(@RequestBody GenericSearchDto genericSearchDto,
                                                        Pageable pageable) {
        return ResponseEntity.ok(taskService.search(genericSearchDto, pageable));
    }
}
