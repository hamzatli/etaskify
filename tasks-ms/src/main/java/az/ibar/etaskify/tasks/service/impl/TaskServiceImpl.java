package az.ibar.etaskify.tasks.service.impl;

import az.ibar.common.dto.GenericSearchDto;
import az.ibar.common.dto.UserResponseDto;
import az.ibar.common.exception.NotFoundException;
import az.ibar.common.search.SearchSpecification;
import az.ibar.common.security.auth.service.SecurityService;
import az.ibar.common.security.exception.UserNotFoundException;
import az.ibar.etaskify.tasks.client.UserManagementClient;
import az.ibar.etaskify.tasks.domain.task.Task;
import az.ibar.etaskify.tasks.domain.task.TaskAssigneeUsers;
import az.ibar.etaskify.tasks.repository.TaskRepository;
import az.ibar.etaskify.tasks.service.TaskService;
import az.ibar.etaskify.tasks.service.dto.TaskRequestDto;
import az.ibar.etaskify.tasks.service.dto.TaskResponseDto;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {

    private final ModelMapper modelMapper;
    private final TaskRepository taskRepository;
    private final UserManagementClient userManagementClient;
    private final SecurityService securityService;
    private final HttpServletRequest servletRequest;

    @Override
    public TaskResponseDto create(TaskRequestDto taskRequestDto) {
        Task task = modelMapper.map(taskRequestDto, Task.class);
        String login = securityService.getCurrentUserLogin().get();
        task.setCreatedBy(login);
        task.setLastModifiedBy(login);
        task.setAssigneeUsers(populateAssigneeUsers(taskRequestDto.getAssigneeUsers(), task));
        return mapTaskToTaskResponseDto(taskRepository.save(task));
    }

    @Override
    public TaskResponseDto assignTask(Long taskId, List<Long> assigneeUserIds) {
        Task task = taskRepository.findById(taskId)
                .orElseThrow(() -> new NotFoundException("Task not found"));
        task.setAssigneeUsers(populateAssigneeUsers(assigneeUserIds, task));
        return mapTaskToTaskResponseDto(taskRepository.save(task));
    }

    @Override
    public Page<TaskResponseDto> search(GenericSearchDto genericSearchDto, Pageable pageable) {
        return taskRepository.findAll(new SearchSpecification<>(genericSearchDto.getCriteria()), pageable)
                .map(this::mapTaskToTaskResponseDto);
    }

    private List<TaskAssigneeUsers> populateAssigneeUsers(List<Long> assigneeUsers, Task task) {
        Boolean check = userManagementClient
                .checkUsersExist(getAuthorizationHeader(), assigneeUsers).getBody();
        //noinspection ConstantConditions
        if (!check) {
            throw new UserNotFoundException();
        }
        List<TaskAssigneeUsers> taskAssigneeUsers = new ArrayList<>();
        assigneeUsers.forEach(l ->
                taskAssigneeUsers.add(TaskAssigneeUsers.builder().task(task).userId(l).build()));
        return taskAssigneeUsers;
    }

    private List<UserResponseDto> populateUserResponseDto(List<TaskAssigneeUsers> assigneeUsers) {
        return userManagementClient.getUsersById(getAuthorizationHeader(), assigneeUsers
                .stream()
                .map(TaskAssigneeUsers::getUserId).collect(Collectors.toList())).getBody();
    }

    private TaskResponseDto mapTaskToTaskResponseDto(Task task) {
        TaskResponseDto responseDto = modelMapper.map(task, TaskResponseDto.class);
        responseDto.setAssigneeUsers(populateUserResponseDto(task.getAssigneeUsers()));
        return responseDto;
    }

    private String getAuthorizationHeader() {
        return servletRequest.getHeader("Authorization");
    }
}
