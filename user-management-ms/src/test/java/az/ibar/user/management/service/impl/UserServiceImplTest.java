package az.ibar.user.management.service.impl;

import az.ibar.common.security.UserAuthority;
import az.ibar.common.security.auth.service.SecurityService;
import az.ibar.common.security.exception.UserNotFoundException;
import az.ibar.user.management.domain.Authority;
import az.ibar.user.management.domain.Organisation;
import az.ibar.user.management.domain.User;
import az.ibar.user.management.domain.enumeration.UserStatus;
import az.ibar.user.management.repository.UserRepository;
import az.ibar.user.management.service.dto.OrganisationRegisterDto;
import az.ibar.user.management.service.dto.RegisterDto;
import az.ibar.user.management.service.dto.response.UserResponseDto;
import az.ibar.user.management.web.rest.dto.ManagedUserDto;
import az.ibar.user.management.web.rest.errors.UsernameAlreadyUsedException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.Duration;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    private static final Long ID = 1L;
    private static final String NAME = "John";
    private static final String SURNAME = "Doe";
    private static final String USERNAME = "johndoe";
    private static final String EMAIL = "john.doe@gmail.com";
    private static final String PASSWORD = "salam123";
    private static final String PASSWORD_ENCODED = "salam123-encoded";
    private static final String USER_DESCRIPTION = "";
    private static final String PHONE = "+994-55-5555555";
    private static final Set<Authority> ADMIN_AUTHORITIES = createAuthorities(UserAuthority.ADMIN.name());
    private static final Set<Authority> MANAGED_AUTHORITIES = createAuthorities(UserAuthority.USER.name());
    private static final UserStatus STATUS = UserStatus.ACTIVE;
    private static final Long ORGANISATION_ID = 1L;
    private static final String ORGANISATION_NAME = "IBA Tech";
    private static final String ORGANISATION_PHONE_NUMBER = "+994-50-5505050";
    private static final String ORGANISATION_ADDRESS = "Baku c.";
    private static final Duration ONE_DAY = Duration.ofDays(1);
    private static final Duration ONE_WEEK = Duration.ofDays(7);

    @Mock
    private UserRepository userRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private SecurityService securityService;

    @Spy
    private ModelMapper modelMapper;

    @InjectMocks
    private UserServiceImpl userService;


    @Test
    void givenInputIsValidWhenRegisterThenSave() {
        // Arrange
        final RegisterDto registerRequestDto = prepareRegisterDto();
        final User user = modelMapper.map(registerRequestDto, User.class);

        when(userRepository.findUserByUsernameIgnoreCase(registerRequestDto.getUsername()))
                .thenReturn(Optional.empty());

        user.setAuthorities(ADMIN_AUTHORITIES);
        user.setStatus(STATUS);

        when(passwordEncoder.encode(any(String.class)))
                .thenReturn(PASSWORD_ENCODED);

        user.setPassword(passwordEncoder.encode(registerRequestDto.getPassword()));

        final User saved = prepareAdminUser();

        when(userRepository.save(user)).thenReturn(saved);

        // Act
        userService.registerUser(registerRequestDto);

        // Assert
        verify(userRepository, times(1))
                .findUserByUsernameIgnoreCase(registerRequestDto.getUsername());
        verify(userRepository, times(1)).save(user);
    }

    @Test
    void givenUsernameAlreadyRegisteredWhenRegisterThenThrowUsernameAlreadyUsedException() {
        // Arrange
        final RegisterDto registerRequestDto = prepareRegisterDto();

        when(userRepository.findUserByUsernameIgnoreCase(registerRequestDto.getUsername()))
                .thenReturn(Optional.of(prepareAdminUser()));

        final String username = registerRequestDto.getUsername();
        final String exceptionMessage = String.format("Username \"%s\" already exist,try different username", username);

        // Act && Assert
        assertThatThrownBy(() -> userService.registerUser(registerRequestDto))
                .isInstanceOf(UsernameAlreadyUsedException.class)
                .hasMessage(exceptionMessage);
        verify(userRepository, times(1))
                .findUserByUsernameIgnoreCase(registerRequestDto.getUsername());
        verify(userRepository, times(0)).save(any(User.class));
    }

    @Test
    void givenInputIsValidWhenCreateUserThenSave() {
        // Arrange
        final ManagedUserDto managedUserDto = prepareManagedUserDto();
        final User user = modelMapper.map(managedUserDto, User.class);

        when(userRepository.findUserByUsernameIgnoreCase(any(String.class)))
                .thenReturn(Optional.empty()).thenReturn(Optional.of(user)); // getCurrentUser

        // getCurrentUser >>>
        when(securityService.getCurrentUserLogin()).thenReturn(Optional.of(managedUserDto.getUsername()));
        // << getCurrentUser

        user.setAuthorities(MANAGED_AUTHORITIES);
        user.setStatus(STATUS);
        user.setOrganisation(Organisation.builder().id(ORGANISATION_ID).build());

        when(passwordEncoder.encode(any(String.class)))
                .thenReturn(PASSWORD_ENCODED);

        user.setPassword(passwordEncoder.encode(managedUserDto.getPassword()));

        final User saved = prepareManagedUser();

        assertThat(saved.getId()).isNotNull();

        when(userRepository.save(user)).thenReturn(saved);


        // Act
        userService.createUser(managedUserDto);

        // Assert
        verify(userRepository, times(2))
                .findUserByUsernameIgnoreCase(managedUserDto.getUsername());
        verify(userRepository, times(1)).save(user);
    }

    @Test
    void givenUsernameAlreadyCreatedWhenCreateUserThenThrowUsernameAlreadyUsedException() {
        // Arrange
        final ManagedUserDto managedUserDto = prepareManagedUserDto();

        when(userRepository.findUserByUsernameIgnoreCase(managedUserDto.getUsername()))
                .thenReturn(Optional.of(prepareAdminUser()));

        final String username = managedUserDto.getUsername();
        final String exceptionMessage = String.format("Username \"%s\" already exist,try different username", username);

        // Act && Assert
        assertThatThrownBy(() -> userService.createUser(managedUserDto))
                .isInstanceOf(UsernameAlreadyUsedException.class)
                .hasMessage(exceptionMessage);
        verify(userRepository, times(1))
                .findUserByUsernameIgnoreCase(managedUserDto.getUsername());
        verify(userRepository, times(0)).save(any(User.class));
    }

    @Test
    void whenGetCurrentUserThenCurrentUser() {
        // Arrange
        final User user = prepareManagedUser();
        when(securityService.getCurrentUserLogin()).thenReturn(Optional.of(user.getUsername()));
        when(userRepository.findUserByUsernameIgnoreCase(any(String.class))).thenReturn(Optional.of(user));

        // Act
        final User result = userService.getCurrentUser();

        // Assert
        verify(userRepository, times(1)).findUserByUsernameIgnoreCase(user.getUsername());
        assertThat(result.getId()).isEqualTo(user.getId());
        assertThat(result.getUsername()).isEqualTo(user.getUsername());
        assertThat(result.getPassword()).isEqualTo(user.getPassword());
        assertThat(result.getName()).isEqualTo(user.getName());
        assertThat(result.getSurname()).isEqualTo(user.getSurname());
        assertThat(result.getUserDescription()).isEqualTo(user.getUserDescription());
        assertThat(result.getPhone()).isEqualTo(user.getPhone());
        assertThat(result.getEmail()).isEqualTo(user.getEmail());
        assertThat(result.getStatus()).isEqualTo(user.getStatus());
        assertThat(result.getAuthorities()).isEqualTo(user.getAuthorities());
        assertThat(result.getOrganisation().getId()).isEqualTo(user.getOrganisation().getId());
        assertThat(result.getOrganisation().getName()).isEqualTo(user.getOrganisation().getName());
        assertThat(result.getOrganisation().getPhoneNumber()).isEqualTo(user.getOrganisation().getPhoneNumber());
        assertThat(result.getOrganisation().getAddress()).isEqualTo(user.getOrganisation().getAddress());
    }

    @Test
    void whenGetCurrentUserThenUserNotFoundException() {
        // Arrange
        final String username = USERNAME;
        when(securityService.getCurrentUserLogin()).thenReturn(Optional.of(username));

        // Act && Assert
        assertThatThrownBy(() -> userService.getCurrentUser())
                .isInstanceOf(UserNotFoundException.class)
                .hasMessage(UserNotFoundException.USER_NOT_FOUND);
        verify(userRepository, times(1)).findUserByUsernameIgnoreCase(username);
    }

    @Test
    void whenGetAllThenResult() {
        // Arrange
        final Pageable pageable = Pageable.unpaged();

        // getCurrentUser >>>
        final User currentUser = prepareAdminUser();

        when(userRepository.findUserByUsernameIgnoreCase(currentUser.getUsername()))
                .thenReturn(Optional.of(currentUser));

        when(securityService.getCurrentUserLogin()).thenReturn(Optional.of(currentUser.getUsername()));
        // <<< getCurrentUser

        when(userRepository
                .findAllByOrganisationIdAndIdNot(currentUser.getOrganisation().getId(), currentUser.getId(), pageable))
                .thenReturn(Page.empty());

        // Act
        userService.getAll(pageable);

        // Assert
        verify(userRepository, times(1)).findUserByUsernameIgnoreCase(currentUser.getUsername());
        verify(userRepository, times(1))
                .findAllByOrganisationIdAndIdNot(currentUser.getOrganisation().getId(), currentUser.getId(), pageable);
    }

    @Test
    void givenIdFoundWhenGetByIdThenResult() {
        // Arrange
        final User user = prepareManagedUser();

        // getCurrentUser >>>
        final User currentUser = prepareAdminUser();

        when(userRepository.findUserByUsernameIgnoreCase(currentUser.getUsername()))
                .thenReturn(Optional.of(currentUser));

        when(securityService.getCurrentUserLogin()).thenReturn(Optional.of(currentUser.getUsername()));
        // <<< getCurrentUser

        when(userRepository.findByIdAndOrganisationId(user.getId(), currentUser.getOrganisation().getId()))
                .thenReturn(Optional.of(user));

        // Act
        UserResponseDto result = userService.getById(user.getId());

        // Assert
        verify(userRepository, times(1)).findUserByUsernameIgnoreCase(currentUser.getUsername());
        verify(userRepository, times(1))
                .findByIdAndOrganisationId(user.getId(), currentUser.getOrganisation().getId());

        assertThat(result.getId()).isEqualTo(user.getId());
    }

    @Test
    void givenIdDoesntMatchAnyUserWhenGetByIdThenThrowUserNotFoundException() {
        // Arrange
        final User user = prepareManagedUser();
        final String exceptionMessage = UserNotFoundException.USER_NOT_FOUND;
        final Long userId = user.getId();

        // getCurrentUser >>>
        final User currentUser = prepareAdminUser();

        when(userRepository.findUserByUsernameIgnoreCase(currentUser.getUsername()))
                .thenReturn(Optional.of(currentUser));

        when(securityService.getCurrentUserLogin()).thenReturn(Optional.of(currentUser.getUsername()));
        // <<< getCurrentUser

        when(userRepository.findByIdAndOrganisationId(userId, currentUser.getOrganisation().getId()))
                .thenReturn(Optional.empty());

        // Act && Assert
        assertThatThrownBy(() -> userService.getById(userId))
                .isInstanceOf(UserNotFoundException.class)
                .hasMessage(exceptionMessage);


        verify(userRepository, times(1)).findUserByUsernameIgnoreCase(currentUser.getUsername());
        verify(userRepository, times(1))
                .findByIdAndOrganisationId(user.getId(), currentUser.getOrganisation().getId());
    }

    @Test
    void whenGetUsersByIdsThenResult() {
        // Arrange
        final List<User> users = List.of(prepareManagedUser());
        final List<Long> ids = List.of(users.get(0).getId());

        // getCurrentUser >>>
        final User currentUser = prepareAdminUser();

        when(userRepository.findUserByUsernameIgnoreCase(currentUser.getUsername()))
                .thenReturn(Optional.of(currentUser));

        when(securityService.getCurrentUserLogin()).thenReturn(Optional.of(currentUser.getUsername()));
        // <<< getCurrentUser

        when(userRepository.findAllByIdInAndOrganisationId(ids, currentUser.getOrganisation().getId()))
                .thenReturn(users);

        // Act
        userService.getUsersByIds(ids);

        // Assert
        verify(userRepository, times(1)).findUserByUsernameIgnoreCase(currentUser.getUsername());
        verify(userRepository, times(1))
                .findAllByIdInAndOrganisationId(ids, currentUser.getOrganisation().getId());
    }

    @Test
    void whenCheckUserExistThenResult() {
        // Arrange
        final List<User> users = List.of(prepareManagedUser());
        final List<Long> ids = List.of(users.get(0).getId());

        // getCurrentUser >>>
        final User currentUser = prepareAdminUser();

        when(userRepository.findUserByUsernameIgnoreCase(currentUser.getUsername()))
                .thenReturn(Optional.of(currentUser));

        when(securityService.getCurrentUserLogin()).thenReturn(Optional.of(currentUser.getUsername()));
        // <<< getCurrentUser

        when(userRepository.findAllByIdInAndOrganisationId(ids, currentUser.getOrganisation().getId()))
                .thenReturn(users);

        // Act
        userService.checkUserExist(ids);

        // Assert
        verify(userRepository, times(1)).findUserByUsernameIgnoreCase(currentUser.getUsername());
        verify(userRepository, times(1))
                .findAllByIdInAndOrganisationId(ids, currentUser.getOrganisation().getId());
    }

    @Test
    void givenIdFoundWhenDeleteThenDelete() {
        // Arrange
        final User userToDelete = prepareManagedUser();

        // getCurrentUser >>>
        final User currentUser = prepareAdminUser();

        when(userRepository.findUserByUsernameIgnoreCase(currentUser.getUsername()))
                .thenReturn(Optional.of(currentUser));

        when(securityService.getCurrentUserLogin()).thenReturn(Optional.of(currentUser.getUsername()));
        // <<< getCurrentUser

        when(userRepository
                .findByIdAndOrganisationIdAndIdNot(
                        userToDelete.getId(),
                        currentUser.getOrganisation().getId(),
                        currentUser.getId()))
                .thenReturn(Optional.of(userToDelete));


        // Act
        userService.delete(userToDelete.getId());

        // Assert
        verify(userRepository, times(1)).findUserByUsernameIgnoreCase(currentUser.getUsername());
        verify(userRepository, times(1))
                .findByIdAndOrganisationIdAndIdNot(
                        userToDelete.getId(),
                        currentUser.getOrganisation().getId(),
                        currentUser.getId());
        verify(userRepository, times(1)).delete(userToDelete);
    }

    private RegisterDto prepareRegisterDto() {
        return RegisterDto
                .builder()
                .name(NAME)
                .surname(SURNAME)
                .username(USERNAME)
                .email(EMAIL)
                .password(PASSWORD)
                .userDescription(USER_DESCRIPTION)
                .phone(PHONE)
                .organisation(
                        OrganisationRegisterDto
                                .builder()
                                .name(ORGANISATION_NAME)
                                .phoneNumber(ORGANISATION_PHONE_NUMBER)
                                .address(ORGANISATION_ADDRESS)
                                .build()
                )
                .build();
    }

    private static Set<Authority> createAuthorities(String... authoritiesString) {
        Set<Authority> authorities = new HashSet<>();
        for (String authorityString : authoritiesString) {
            authorities.add(new Authority(authorityString));
        }
        return authorities;
    }

    private User prepareAdminUser() {
        return User
                .builder()
                .id(ID)
                .username(USERNAME)
                .password(PASSWORD_ENCODED)
                .name(NAME)
                .surname(SURNAME)
                .userDescription(USER_DESCRIPTION)
                .phone(PHONE)
                .email(EMAIL)
                .status(STATUS)
                .authorities(ADMIN_AUTHORITIES)
                .organisation(
                        Organisation
                                .builder()
                                .id(ORGANISATION_ID)
                                .name(ORGANISATION_NAME)
                                .phoneNumber(ORGANISATION_PHONE_NUMBER)
                                .address(ORGANISATION_ADDRESS)
                                .build()
                )
                .build();
    }

    private ManagedUserDto prepareManagedUserDto() {
        return ManagedUserDto
                .builder()
                .name(NAME)
                .surname(SURNAME)
                .username(USERNAME)
                .email(EMAIL)
                .password(PASSWORD)
                .userDescription(USER_DESCRIPTION)
                .phone(PHONE)
                .build();
    }

    private User prepareManagedUser() {
        return User
                .builder()
                .id(ID)
                .username(USERNAME)
                .password(PASSWORD_ENCODED)
                .name(NAME)
                .surname(SURNAME)
                .userDescription(USER_DESCRIPTION)
                .phone(PHONE)
                .email(EMAIL)
                .status(STATUS)
                .authorities(MANAGED_AUTHORITIES)
                .organisation(
                        Organisation
                                .builder()
                                .id(ORGANISATION_ID)
                                .name(ORGANISATION_NAME)
                                .phoneNumber(ORGANISATION_PHONE_NUMBER)
                                .address(ORGANISATION_ADDRESS)
                                .build()
                )
                .build();
    }

}