package az.ibar.user.management.service.impl;

import az.ibar.common.security.auth.service.SecurityService;
import az.ibar.user.management.domain.User;
import az.ibar.user.management.domain.enumeration.UserStatus;
import az.ibar.user.management.repository.UserRepository;
import az.ibar.user.management.service.dto.ChangePasswordDto;
import az.ibar.user.management.web.rest.errors.NewPasswordsNotMatchException;
import az.ibar.user.management.web.rest.errors.PasswordIsIncorrectException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PasswordServiceImplTest {

    private static final Long ID = 1L;
    private static final String NAME = "John";
    private static final String SURNAME = "Doe";
    private static final String USERNAME = "johndoe";
    private static final String EMAIL = "john.doe@gmail.com";
    private static final String PASSWORD = "salam123";
    private static final String PASSWORD_ENCODED = "salam123-encoded";
    private static final String INCORRECT_PASSWORD = "salam456";
    private static final String NEW_PASSWORD = "salam123-upd";
    private static final String NEW_PASSWORD_ENCODED = "salam123-upd-encoded";
    private static final String USER_DESCRIPTION = "";
    private static final String PHONE = "+994-55-5555555";
    private static final UserStatus STATUS = UserStatus.ACTIVE;

    @Mock
    private UserRepository userRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private SecurityService securityService;

    @InjectMocks
    private PasswordServiceImpl passwordService;


    @Test
    void givenInputIsValidWhenChangePasswordThenSave() {
        // Arrange
        final ChangePasswordDto changePasswordDto = prepareChangePasswordDto();
        final String username = USERNAME;
        final User user = prepareUser();

        when(securityService.getCurrentUserLogin()).thenReturn(Optional.of(username));
        when(userRepository.findUserByUsernameIgnoreCase(username)).thenReturn(Optional.of(user));

        when(passwordEncoder.matches(any(String.class), any(String.class))).thenReturn(true);

        final String newPassword = NEW_PASSWORD_ENCODED;
        when(passwordEncoder.encode(changePasswordDto.getNewPassword())).thenReturn(newPassword);

        user.setPassword(newPassword);

        when(userRepository.save(user)).thenReturn(user);

        // Act
        passwordService.changePassword(changePasswordDto);

        // Assert
        verify(userRepository, times(1)).findUserByUsernameIgnoreCase(username);
        verify(userRepository, times(1)).save(user);
    }

    @Test
    void givenOldPasswordIsIncorrectWhenChangePasswordThenThrowPasswordIsIncorrectException() {
        // Arrange
        final ChangePasswordDto changePasswordDto = prepareChangePasswordDtoWithIncorrectOldPassword();
        final String username = USERNAME;
        final User user = prepareUser();

        when(securityService.getCurrentUserLogin()).thenReturn(Optional.of(username));
        when(userRepository.findUserByUsernameIgnoreCase(username)).thenReturn(Optional.of(user));

        when(passwordEncoder.matches(any(String.class), any(String.class))).thenReturn(false);

        // Act && Assert
        assertThatThrownBy(() -> passwordService.changePassword(changePasswordDto))
                .isInstanceOf(PasswordIsIncorrectException.class)
                .hasMessage(PasswordIsIncorrectException.OLD_PASSWORD_NOT_CORRECT);

        verify(userRepository, times(1)).findUserByUsernameIgnoreCase(username);
        verify(userRepository, times(0)).save(user);
    }

    @Test
    void givenMismatchedNewPasswordsWhenChangePasswordThenThrowPasswordIsIncorrectException() {
        // Arrange
        final ChangePasswordDto changePasswordDto = prepareChangePasswordDtoWithMismatchedNewPasswords();
        final String username = USERNAME;
        final User user = prepareUser();

        when(securityService.getCurrentUserLogin()).thenReturn(Optional.of(username));
        when(userRepository.findUserByUsernameIgnoreCase(username)).thenReturn(Optional.of(user));

        when(passwordEncoder.matches(any(String.class), any(String.class))).thenReturn(true);

        // Act && Assert
        assertThatThrownBy(() -> passwordService.changePassword(changePasswordDto))
                .isInstanceOf(NewPasswordsNotMatchException.class)
                .hasMessage(NewPasswordsNotMatchException.PASSWORDS_DO_NOT_MATCH);

        verify(userRepository, times(1)).findUserByUsernameIgnoreCase(username);
        verify(userRepository, times(0)).save(user);
    }

    private ChangePasswordDto prepareChangePasswordDto() {
        return ChangePasswordDto
                .builder()
                .oldPassword(PASSWORD)
                .newPassword(NEW_PASSWORD)
                .repeatPassword(NEW_PASSWORD)
                .build();
    }

    private ChangePasswordDto prepareChangePasswordDtoWithIncorrectOldPassword() {
        return ChangePasswordDto
                .builder()
                .oldPassword(INCORRECT_PASSWORD)
                .newPassword(NEW_PASSWORD)
                .repeatPassword(NEW_PASSWORD)
                .build();
    }

    private ChangePasswordDto prepareChangePasswordDtoWithMismatchedNewPasswords() {
        return ChangePasswordDto
                .builder()
                .oldPassword(PASSWORD)
                .newPassword(NEW_PASSWORD)
                .repeatPassword(INCORRECT_PASSWORD)
                .build();
    }

    private User prepareUser() {
        return User
                .builder()
                .id(ID)
                .username(USERNAME)
                .password(PASSWORD_ENCODED)
                .name(NAME)
                .surname(SURNAME)
                .userDescription(USER_DESCRIPTION)
                .phone(PHONE)
                .email(EMAIL)
                .status(STATUS)
                .build();
    }

}
