package az.ibar.user.management.service.web.rest;

import az.ibar.user.management.service.UserService;
import az.ibar.user.management.service.dto.response.OrganisationResponseDto;
import az.ibar.user.management.service.dto.response.UserResponseDto;
import az.ibar.user.management.web.rest.UserController;
import az.ibar.user.management.web.rest.dto.ManagedUserDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(UserController.class)
class UserControllerTest {

    private static final String BASE_URL = "/v1/users";
    private static final String CONTENT_TYPE = "application/json";
    private static final String ROLE_ADMIN = "ADMIN";

    private static final Long ID = 1L;
    private static final String NAME = "John";
    private static final String SURNAME = "Doe";
    private static final String USERNAME = "johndoe";
    private static final String EMAIL = "john.doe@gmail.com";
    private static final String PASSWORD = "salam123";
    private static final String USER_DESCRIPTION = "";
    private static final String PHONE = "+994-55-5555555";
    private static final Long ORGANISATION_ID = 1L;
    private static final String ORGANISATION_NAME = "IBA Tech";
    private static final String ORGANISATION_PHONE_NUMBER = "+994-50-5505050";
    private static final String ORGANISATION_ADDRESS = "Baku c.";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private UserService userService;


    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void givenInputIsValidWhenCreateUserThenIsCreated() throws Exception {
        // Arrange
        final ManagedUserDto managedUserDto = prepareManagedUserDto();

        // Act
        ResultActions actions = mockMvc.perform(
                post(BASE_URL)
                        .contentType(CONTENT_TYPE)
                        .content(objectMapper.writeValueAsString(managedUserDto)));

        // Assert
        verify(userService, times(1)).createUser(managedUserDto);
        actions.andExpect(status().isCreated());
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void whenGetAllThenIsOk() throws Exception {
        // Arrange
        final Pageable pageable = Pageable.unpaged();
        final Page<UserResponseDto> page = Page.empty();
        when(userService.getAll(pageable)).thenReturn(page);

        // Action
        ResultActions actions = mockMvc.perform(
                get(BASE_URL)
                        .contentType(CONTENT_TYPE));

        // Assert
        actions.andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void givenIdFoundWhenGetByIdThenIsOk() throws Exception {
        // Arrange
        final UserResponseDto userResponseDto = prepareUserResponseDto();
        final Long userId = ID;
        when(userService.getById(ID)).thenReturn(userResponseDto);

        // Action
        ResultActions actions = mockMvc.perform(
                get(BASE_URL + "/" + userId)
                        .contentType(CONTENT_TYPE));

        // Assert
        verify(userService, times(1)).getById(userId);

        actions.andExpect(status().isOk());
        actions.andExpect(jsonPath("id", is(userId), Long.class));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void whenGetUsersByIdsThenIsOk() throws Exception {
        // Arrange
        final List<az.ibar.common.dto.UserResponseDto> userResponseDtoSet = List.of(prepareUserResponseDtoCommon());
        final Long userId = ID;
        final List<Long> userIdsList = List.of(userId);
        when(userService.getUsersByIds(userIdsList)).thenReturn(userResponseDtoSet);

        // Action
        ResultActions actions = mockMvc.perform(
                get(BASE_URL + "/bulk")
                        .param("ids", userId.toString())
                        .contentType(CONTENT_TYPE));

        // Assert
        verify(userService, times(1)).getUsersByIds(userIdsList);

        actions.andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void whenCheckUserExistThenIsOk() throws Exception {
        // Arrange
        final Boolean result = Boolean.TRUE;
        final Long userId = ID;
        final List<Long> userIdsList = List.of(userId);
        when(userService.checkUserExist(userIdsList)).thenReturn(result);

        // Action
        ResultActions actions = mockMvc.perform(
                get(BASE_URL + "/check")
                        .param("ids", userId.toString())
                        .contentType(CONTENT_TYPE));

        // Assert
        verify(userService, times(1)).checkUserExist(userIdsList);

        actions.andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void givenIdFoundWhenDeleteThenIsNoContent() throws Exception {
        // Arrange
        final Long userId = ID;

        // Action
        ResultActions actions = mockMvc.perform(
                delete(BASE_URL + "/" + userId)
                        .contentType(CONTENT_TYPE));

        // Assert
        verify(userService, times(1)).delete(userId);

        actions.andExpect(status().isNoContent());
    }

    private ManagedUserDto prepareManagedUserDto() {
        return ManagedUserDto
                .builder()
                .name(NAME)
                .surname(SURNAME)
                .username(USERNAME)
                .email(EMAIL)
                .password(PASSWORD)
                .userDescription(USER_DESCRIPTION)
                .phone(PHONE)
                .build();
    }

    private UserResponseDto prepareUserResponseDto() {
        return UserResponseDto
                .builder()
                .id(ID)
                .name(NAME)
                .surname(SURNAME)
                .email(EMAIL)
                .username(USERNAME)
                .phone(PHONE)
                .userDescription(USER_DESCRIPTION)
                .organisation(OrganisationResponseDto
                        .builder()
                        .id(ORGANISATION_ID)
                        .name(ORGANISATION_NAME)
                        .address(ORGANISATION_ADDRESS)
                        .phoneNumber(ORGANISATION_PHONE_NUMBER)
                        .build())
                .build();
    }

    private az.ibar.common.dto.UserResponseDto prepareUserResponseDtoCommon() {
        return az.ibar.common.dto.UserResponseDto
                .builder()
                .id(ID)
                .name(NAME)
                .email(EMAIL)
                .build();
    }

}