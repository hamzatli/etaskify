package az.ibar.user.management.service.web.rest;

import az.ibar.user.management.service.PasswordService;
import az.ibar.user.management.service.dto.ChangePasswordDto;
import az.ibar.user.management.web.rest.PasswordController;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(PasswordController.class)
class PasswordControllerTest {

    private static final String BASE_URL = "/v1/account/password";
    private static final String CONTENT_TYPE = "application/json";
    private static final String ROLE_USER = "USER";

    private static final String PASSWORD = "salam123";
    private static final String NEW_PASSWORD = "salam123-upd";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private PasswordService passwordService;


    @Test
    @WithMockUser(authorities = ROLE_USER)
    void givenInputIsValidWhenChangePasswordThenIsOk() throws Exception {
        // Arrange
        final ChangePasswordDto changePasswordDto = prepareChangePasswordDto();

        // Act
        ResultActions actions = mockMvc.perform(
                put(BASE_URL + "/change")
                        .contentType(CONTENT_TYPE)
                        .content(objectMapper.writeValueAsString(changePasswordDto)));

        // Assert
        verify(passwordService, times(1)).changePassword(changePasswordDto);
        actions.andExpect(status().isOk());
    }

    private ChangePasswordDto prepareChangePasswordDto() {
        return ChangePasswordDto
                .builder()
                .oldPassword(PASSWORD)
                .newPassword(NEW_PASSWORD)
                .repeatPassword(NEW_PASSWORD)
                .build();
    }

}