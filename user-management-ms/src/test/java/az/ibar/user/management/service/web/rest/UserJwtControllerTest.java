package az.ibar.user.management.service.web.rest;

import az.ibar.user.management.service.UserService;
import az.ibar.user.management.service.dto.OrganisationRegisterDto;
import az.ibar.user.management.service.dto.RegisterDto;
import az.ibar.user.management.web.rest.UserJwtController;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(UserJwtController.class)
class UserJwtControllerTest {

    private static final String BASE_URL = "/v1/auth";
    private static final String CONTENT_TYPE = "application/json";

    private static final String NAME = "John";
    private static final String SURNAME = "Doe";
    private static final String USERNAME = "johndoe";
    private static final String EMAIL = "john.doe@gmail.com";
    private static final String PASSWORD = "salam123";
    private static final String USER_DESCRIPTION = "";
    private static final String PHONE = "+994-55-5555555";
    private static final String ORGANISATION_NAME = "IBA Tech";
    private static final String ORGANISATION_PHONE_NUMBER = "+994-50-5505050";
    private static final String ORGANISATION_ADDRESS = "Baku c.";


    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private UserService userService;


    @Test
    void authorize() {
    }

    @Test
    void givenInputIsValidWhenRegisterThenIsOk() throws Exception {
        // Arrange
        final RegisterDto registerDto = prepareRegisterDto();

        // Act
        ResultActions actions = mockMvc.perform(
                post(BASE_URL + "/register")
                        .contentType(CONTENT_TYPE)
                        .content(objectMapper.writeValueAsString(registerDto)));

        // Assert
        verify(userService, times(1)).registerUser(registerDto);
        actions.andExpect(status().isOk());
    }

    private RegisterDto prepareRegisterDto() {
        return RegisterDto
                .builder()
                .name(NAME)
                .surname(SURNAME)
                .username(USERNAME)
                .email(EMAIL)
                .password(PASSWORD)
                .userDescription(USER_DESCRIPTION)
                .phone(PHONE)
                .organisation(
                        OrganisationRegisterDto
                                .builder()
                                .name(ORGANISATION_NAME)
                                .phoneNumber(ORGANISATION_PHONE_NUMBER)
                                .address(ORGANISATION_ADDRESS)
                                .build()
                )
                .build();
    }

}