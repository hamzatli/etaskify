package az.ibar.user.management.config;

import az.ibar.common.config.ModelMapperConfig;
import az.ibar.common.config.SpringFoxConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@Import({SpringFoxConfig.class, ModelMapperConfig.class})
public class CommonConfig {

}
