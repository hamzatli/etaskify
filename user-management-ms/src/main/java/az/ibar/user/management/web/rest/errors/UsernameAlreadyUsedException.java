package az.ibar.user.management.web.rest.errors;

import az.ibar.common.exception.InvalidStateException;

public class UsernameAlreadyUsedException extends InvalidStateException {

    private static final long serialVersionUID = 1L;

    public UsernameAlreadyUsedException(String username) {
        super(String.format("Username \"%s\" already exist,try different username", username));
    }
}
