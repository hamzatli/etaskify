package az.ibar.user.management.service;

import az.ibar.user.management.service.dto.RegisterDto;
import az.ibar.user.management.service.dto.response.UserResponseDto;
import az.ibar.user.management.web.rest.dto.ManagedUserDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UserService {

    void registerUser(RegisterDto registerDto);

    void createUser(ManagedUserDto userVm);

    Page<UserResponseDto> getAll(Pageable pageable);

    UserResponseDto getById(Long id);

    void delete(Long id);

    Boolean checkUserExist(List<Long> userIds);

    List<az.ibar.common.dto.UserResponseDto> getUsersByIds(List<Long> ids);

}
