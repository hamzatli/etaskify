package az.ibar.user.management.service;

import az.ibar.user.management.service.dto.ChangePasswordDto;

public interface PasswordService {

    void changePassword(ChangePasswordDto dto);
}
