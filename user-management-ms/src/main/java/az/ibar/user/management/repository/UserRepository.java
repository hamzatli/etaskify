package az.ibar.user.management.repository;

import az.ibar.user.management.domain.User;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @EntityGraph(attributePaths = "authorities")
    @Query("select u from User u where lower(u.username)=lower(:username)")
    Optional<User> findOneWithAuthoritiesByUsername(@Param("username") String login);

    Optional<User> findUserByUsernameIgnoreCase(String username);

    Optional<User> findByEmail(String email);

    Page<User> findAllByOrganisationIdAndIdNot(Long organisationId, Long currentUserId, Pageable pageable);

    List<User> findAllByIdInAndOrganisationId(List<Long> ids, Long organisationId);

    Optional<User> findByIdAndOrganisationId(Long id, Long organisationId);

    Optional<User> findByIdAndOrganisationIdAndIdNot(Long id, Long organisationId, Long currentUserId);
}
