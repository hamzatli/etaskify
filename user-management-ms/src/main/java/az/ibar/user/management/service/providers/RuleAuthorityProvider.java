package az.ibar.user.management.service.providers;

import az.ibar.common.security.auth.service.Claim;
import az.ibar.common.security.auth.service.ClaimProvider;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class RuleAuthorityProvider implements ClaimProvider {

    private static final String RULE = "rule";

    @Override
    public Claim provide(Authentication authentication) {
        log.trace("Providing claims");
        Set<String> authorities = authentication.getAuthorities().stream()
                .map(Object::toString)
                .collect(Collectors.toSet());
        return new Claim(RULE, authorities);
    }
}
