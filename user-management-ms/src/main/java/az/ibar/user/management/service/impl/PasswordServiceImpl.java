package az.ibar.user.management.service.impl;

import az.ibar.common.security.auth.service.SecurityService;
import az.ibar.user.management.repository.UserRepository;
import az.ibar.user.management.service.dto.ChangePasswordDto;
import az.ibar.user.management.web.rest.errors.NewPasswordsNotMatchException;
import az.ibar.user.management.web.rest.errors.PasswordIsIncorrectException;
import az.ibar.user.management.service.PasswordService;

import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Import;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@Import(SecurityService.class)
@RequiredArgsConstructor
public class PasswordServiceImpl implements PasswordService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final SecurityService securityService;

    @Override
    public void changePassword(ChangePasswordDto dto) {
        Optional<String> currentUserLogin = securityService.getCurrentUserLogin();
        userRepository.findUserByUsernameIgnoreCase(currentUserLogin.get()).ifPresent(
                user -> {
                    if (!passwordEncoder.matches(dto.getOldPassword(), user.getPassword())) {
                        throw new PasswordIsIncorrectException();
                    }
                    checkIfNewPasswordsMatch(dto.getNewPassword(), dto.getRepeatPassword());
                    user.setPassword(passwordEncoder.encode(dto.getNewPassword()));
                    userRepository.save(user);
                });
    }

    private void checkIfNewPasswordsMatch(String password, String repeatPassword) {
        if (!password.equals(repeatPassword)) {
            throw new NewPasswordsNotMatchException();
        }
    }

}
