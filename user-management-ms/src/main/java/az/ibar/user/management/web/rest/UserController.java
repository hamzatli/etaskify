package az.ibar.user.management.web.rest;

import az.ibar.user.management.service.UserService;
import az.ibar.user.management.service.dto.response.UserResponseDto;
import az.ibar.user.management.web.rest.dto.ManagedUserDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/v1/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping
    public ResponseEntity<Void> createUser(@RequestBody @Valid ManagedUserDto managedUserDto) {
        userService.createUser(managedUserDto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping
    public ResponseEntity<Page<UserResponseDto>> getAll(Pageable pageable) {
        log.trace("Get all users: {}", pageable);
        return ResponseEntity.ok(userService.getAll(pageable));
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserResponseDto> getById(@PathVariable Long id) {
        log.trace("Get user #{}", id);
        return ResponseEntity.ok(userService.getById(id));
    }

    @GetMapping("/bulk")
    public ResponseEntity<List<az.ibar.common.dto.UserResponseDto>> getUsersById(@RequestParam List<Long> ids) {
        return ResponseEntity.ok(userService.getUsersByIds(ids));
    }

    @GetMapping("/check")
    public ResponseEntity<Boolean> checkUserExist(@RequestParam List<Long> ids) {
        return ResponseEntity.ok(userService.checkUserExist(ids));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.trace("Delete user #{}", id);

        userService.delete(id);
        return ResponseEntity.noContent().build();
    }

}
