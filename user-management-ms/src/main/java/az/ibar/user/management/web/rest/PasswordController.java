package az.ibar.user.management.web.rest;

import az.ibar.user.management.service.PasswordService;
import az.ibar.user.management.service.dto.ChangePasswordDto;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/account/password")
@RequiredArgsConstructor
public class PasswordController {

    private final PasswordService passwordService;

    @PutMapping("/change")
    public void changePassword(@RequestBody @Valid ChangePasswordDto dto) {
        passwordService.changePassword(dto);
    }
}
