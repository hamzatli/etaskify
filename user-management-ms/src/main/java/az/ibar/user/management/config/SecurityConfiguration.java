package az.ibar.user.management.config;

import az.ibar.common.security.UserAuthority;
import az.ibar.common.security.auth.AuthenticationEntryPointConfigurer;
import az.ibar.common.security.auth.service.JwtService;
import az.ibar.common.security.auth.service.TokenAuthService;
import az.ibar.common.security.config.BaseSecurityConfig;
import az.ibar.common.security.config.SecurityProperties;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Slf4j
@EnableWebSecurity
@Import({SecurityProperties.class, JwtService.class,
        AuthenticationEntryPointConfigurer.class})
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfiguration extends BaseSecurityConfig {

    public SecurityConfiguration(SecurityProperties securityProperties, JwtService jwtService) {
        super(securityProperties, List.of(new TokenAuthService(jwtService)));
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/v1/auth/**").permitAll()
                .antMatchers("/v1/account/**").authenticated()
                .antMatchers("/v1/users/**").access(authorities(UserAuthority.ADMIN));
        super.configure(http);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
