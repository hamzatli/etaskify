package az.ibar.user.management.service.impl;

import az.ibar.common.security.UserAuthority;
import az.ibar.common.security.auth.service.SecurityService;
import az.ibar.common.security.exception.UserNotFoundException;
import az.ibar.user.management.domain.Authority;
import az.ibar.user.management.domain.User;
import az.ibar.user.management.domain.enumeration.UserStatus;
import az.ibar.user.management.repository.UserRepository;
import az.ibar.user.management.service.UserService;
import az.ibar.user.management.service.dto.RegisterDto;
import az.ibar.user.management.service.dto.response.UserResponseDto;
import az.ibar.user.management.web.rest.dto.ManagedUserDto;
import az.ibar.user.management.web.rest.errors.UsernameAlreadyUsedException;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final ModelMapper modelMapper;
    private final SecurityService securityService;

    @Override
    public void registerUser(RegisterDto registerDto) {
        userRepository.findUserByUsernameIgnoreCase(registerDto.getUsername())
                .ifPresent(user -> {
                    throw new UsernameAlreadyUsedException(registerDto.getUsername());
                });
        User user = modelMapper.map(registerDto, User.class);
        user.setAuthorities(createAuthorities(UserAuthority.ADMIN.name()));
        user.setStatus(UserStatus.ACTIVE);
        user.setPassword(passwordEncoder.encode(registerDto.getPassword()));
        userRepository.save(user);
    }

    @Override
    public void createUser(ManagedUserDto managedUserDto) {
        userRepository.findUserByUsernameIgnoreCase(managedUserDto.getUsername())
                .ifPresent(user -> {
                    throw new UsernameAlreadyUsedException(managedUserDto.getUsername());
                });
        User user = modelMapper.map(managedUserDto, User.class);
        user.setAuthorities(createAuthorities(UserAuthority.USER.name()));
        user.setStatus(UserStatus.ACTIVE);
        user.setPassword(passwordEncoder.encode(managedUserDto.getPassword()));
        user.setOrganisation(getCurrentUser().getOrganisation());
        userRepository.save(user);
    }

    @Override
    public Page<UserResponseDto> getAll(Pageable pageable) {
        User currentUser = getCurrentUser();
        return userRepository
                .findAllByOrganisationIdAndIdNot(currentUser.getOrganisation().getId(), currentUser.getId(), pageable)
                .map(user -> modelMapper.map(user, UserResponseDto.class));
    }

    @Override
    public UserResponseDto getById(Long id) {
        User currentUser = getCurrentUser();
        return userRepository
                .findByIdAndOrganisationId(id, currentUser.getOrganisation().getId())
                .map(user -> modelMapper.map(user, UserResponseDto.class))
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    public List<az.ibar.common.dto.UserResponseDto> getUsersByIds(List<Long> ids) {
        User currentUser = getCurrentUser();

        return userRepository
                .findAllByIdInAndOrganisationId(ids, currentUser.getOrganisation().getId())
                .stream()
                .map(user -> modelMapper.map(user, az.ibar.common.dto.UserResponseDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public Boolean checkUserExist(List<Long> userIds) {
        User currentUser = getCurrentUser();

        return userRepository
                .findAllByIdInAndOrganisationId(userIds, currentUser.getOrganisation().getId()).size() == userIds.size();
    }

    @Override
    public void delete(Long id) {
        User currentUser = getCurrentUser();

        User user = userRepository
                .findByIdAndOrganisationIdAndIdNot(id, currentUser.getOrganisation().getId(), currentUser.getId())
                .orElseThrow(UserNotFoundException::new);

        userRepository.delete(user);
    }

    @SuppressWarnings("OptionalGetWithoutIsPresent")
    public User getCurrentUser() {
        return userRepository.findUserByUsernameIgnoreCase(securityService.getCurrentUserLogin().get())
                .orElseThrow(UserNotFoundException::new);
    }

    private Set<Authority> createAuthorities(String... authoritiesString) {
        Set<Authority> authorities = new HashSet<>();
        for (String authorityString : authoritiesString) {
            authorities.add(new Authority(authorityString));
        }
        return authorities;
    }

}
