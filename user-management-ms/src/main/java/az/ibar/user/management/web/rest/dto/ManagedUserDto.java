package az.ibar.user.management.web.rest.dto;

import az.ibar.user.management.config.PasswordConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ManagedUserDto {

    @NotBlank
    private String name;

    @NotBlank
    private String surname;

    @NotBlank
    private String username;

    @Email
    private String email;

    @Size(max = PasswordConstants.PASSWORD_MAX_LENGTH, min = PasswordConstants.PASSWORD_MIN_LENGTH)
    private String password;

    private String userDescription;

    @NotBlank
    private String phone;

}
