package az.ibar.user.management.service.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserResponseDto {

    private Long id;
    private String name;
    private String surname;
    private String username;
    private String email;
    private String userDescription;
    private String phone;
    private OrganisationResponseDto organisation;

}
