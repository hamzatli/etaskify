package az.ibar.user.management.service.dto;

import az.ibar.user.management.config.PasswordConstants;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChangePasswordDto {

    @NotNull
    private String oldPassword;

    @NotNull
    @Size(max = PasswordConstants.PASSWORD_MAX_LENGTH, min = PasswordConstants.PASSWORD_MIN_LENGTH)
    private String newPassword;

    @NotNull
    @Size(max = PasswordConstants.PASSWORD_MAX_LENGTH, min = PasswordConstants.PASSWORD_MIN_LENGTH)
    private String repeatPassword;

}
