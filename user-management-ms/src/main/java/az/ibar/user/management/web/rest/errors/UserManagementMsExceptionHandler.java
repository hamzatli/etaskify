package az.ibar.user.management.web.rest.errors;

import az.ibar.common.exception.GlobalExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class UserManagementMsExceptionHandler extends GlobalExceptionHandler {

}
