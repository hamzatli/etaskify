package az.ibar.user.management.service.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class OrganisationResponseDto {

    private Long id;
    private String name;
    private String phoneNumber;
    private String address;

}
