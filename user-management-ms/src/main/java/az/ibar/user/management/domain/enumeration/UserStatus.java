package az.ibar.user.management.domain.enumeration;

public enum UserStatus {
    ACTIVE, DELETED, BLOCKED
}
