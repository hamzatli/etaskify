package az.ibar.common.security;

public enum UserAuthority {
    USER, ADMIN, SUPER_USER, ANONYMOUS, PARTNER, AGENT, EDITOR
}
